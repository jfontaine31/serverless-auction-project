const AWS = require("aws-sdk");
const sqs = new AWS.SQS();
const { MAIL_QUEUE_URL } = require('../constants/sqs');

class SqsHandler {
    /**
     *
     * @param messageBody
     * @returns {Promise<PromiseResult<SQS.SendMessageResult, AWSError>>}
     */
    sendMessage(messageBody) {
        return sqs.sendMessage({
            QueueUrl: MAIL_QUEUE_URL,
            MessageBody: JSON.stringify(messageBody)
        }).promise();
    }
}

module.exports = SqsHandler;