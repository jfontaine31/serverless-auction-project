const AWS = require("aws-sdk");
/** @type DocumentClient */
const dynamoDB = new AWS.DynamoDB.DocumentClient();

module.exports = dynamoDB;