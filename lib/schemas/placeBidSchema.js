const schema = {
    properties: {
        body: {
            type: "object",
            properties: {
                amount: {
                    type: "number",
                    default: 0
                }
            },
            // required: ["amount"]
        }
    },
    required: [
        "body",
    ]
}

module.exports = schema;
