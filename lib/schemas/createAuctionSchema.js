const schema = {
    properties: {
        body: {
            type: "object",
            properties: {
                title: {
                    type: "string",
                }
            },
            required: ["title"]
        },
        // requestContext: {
        //     type: "object",
        //     properties: {
        //         authorizer: {
        //             type: "object",
        //             properties: {
        //                 email: {
        //                     type: "string"
        //                 }
        //             }
        //         }
        //     },
        //     required: ["title"]
        // },
    },
    required: ["body"]
}

module.exports = schema;
