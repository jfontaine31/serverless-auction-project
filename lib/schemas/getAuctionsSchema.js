const AUCTION_STATUSES = require("../../constants/auction-statuses");
const schema = {
    properties: {
        queryStringParameters: {
            type: "object",
            properties: {
                status: {
                    type: "string",
                    enum: [AUCTION_STATUSES.OPEN, AUCTION_STATUSES.CLOSED],
                    default: AUCTION_STATUSES.OPEN
                }
            }
        },
    },
    required: [
        "queryStringParameters"
    ]
}

module.exports = schema;
