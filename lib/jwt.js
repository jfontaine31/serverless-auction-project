const jwt = require('jsonwebtoken');
const { AUTH0_PUBLIC_KEY } = require('../constants/AUTH0')

/**
 * Wraps jsonwebtoken functions to make them async instead of sync
 */
class Jwt {
    static verify(token) {
        return new Promise((resolve, reject) => {
            jwt.verify(token, AUTH0_PUBLIC_KEY, (err, res) => {
                if(err) {
                    console.error(err);
                    reject(err);
                }
                else {
                    resolve(res);
                }

            });
        });
    }
}

module.exports = Jwt;
