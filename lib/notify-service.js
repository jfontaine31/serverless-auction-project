const SqsHandler = require('../lib/sqs');
const NoAuctionError = require('../lib/errors/no-auction-error')

class NotifyService {

    static notifySuccessfulAuction(auction) {
        const {
            title,
            seller,
            highestBid: {amount, bidder}
        } = auction;

        if(amount === 0 && !bidder) {
            throw new NoAuctionError("Invalid Auction", auction);
        }

        const promises = NotifyService.notifyParties(seller, title, amount, bidder)
        return Promise.all(promises);

    }

     static async notifyNoSeller(auction) {
        const { title, seller } = auction;
        const sqsHandler = new SqsHandler();
        const sellerParam = {
            subject: `Your Item ${title} has not been sold`,
            recipient: seller,
            body: `No one bid on your item ${title}`
        };
        await sqsHandler.sendMessage(sellerParam);
        return false;
    }

     static notifyParties(seller, title, amount, bidder) {
        const sqsHandler = new SqsHandler()
        const sellerParam = {
            subject: 'Your Item has been sold',
            recipient: seller,
            body: `Your Item ${title} has been sold for $${amount}`
        };
        const bidderParam = {
            subject: 'You won the auction',
            recipient: bidder,
            body: `What a great deal you got yourself a ${title} for $${amount}`
        };
        const notifySellerPromise = sqsHandler.sendMessage(sellerParam);
        const notifyBidderPromise = sqsHandler.sendMessage(bidderParam)
        return [notifySellerPromise, notifyBidderPromise];
    }
}

module.exports = NotifyService;