const middy = require('@middy/core');
const httpJsonBodyParser = require('@middy/http-json-body-parser');
const httpEventNormalizer = require('@middy/http-event-normalizer');
const httpErrorHandler = require('@middy/http-error-handler');

const handler = handlerFunction => middy(handlerFunction)
    .use([
        httpJsonBodyParser(), // automatically parses event.body
        httpEventNormalizer(), // remove room for errors
        httpErrorHandler() //Creates a proper HTTP response with http-errors
    ]);

module.exports = handler;