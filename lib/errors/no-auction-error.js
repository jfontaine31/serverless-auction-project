

class NoAuctionError extends Error {
    constructor(message, auction) {
        super(message);
        this.auction = auction;
    }
}

module.exports = NoAuctionError;
