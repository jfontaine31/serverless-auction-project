const statuses = require('statuses');
const createError = require('http-errors');
const validator = require("@middy/validator");
const commonMiddleware = require("../../lib/common-middlewares")
const createAuctionsSchema = require('../../lib/schemas/createAuctionSchema');

const Auctions = require('../../controllers/auctions');


async function createAuction(event, context) {
  const { title } = event.body;
  const { email } = event.requestContext.authorizer;
  try {
    const auctions = new Auctions();
    const auctionResult = await auctions.createAuction(title, email);

    return {
      statusCode: statuses("Created"),
      body: JSON.stringify({ auction: auctionResult }),
    };
  }
  catch(error) {
    console.error(error);
    throw new createError.InternalServerError(error);

  }
}

module.exports.handler = commonMiddleware(createAuction)
    .use(validator({ inputSchema: createAuctionsSchema }));
