const statuses = require('statuses');
const validator = require("@middy/validator");
const commonMiddleware = require("../../lib/common-middlewares");
const createError = require('http-errors');
const getAuctionsSchema = require('../../lib/schemas/getAuctionsSchema');

const Auctions = require("../../controllers/auctions");

async function getAuctions(event, context) {
    const { status } = event.queryStringParameters;
    try {
        const auctions = new Auctions();
        const auctionsResult = await auctions.getAuctionsByStatus(status);
        return {
            statusCode: statuses("OK"),
            body: JSON.stringify(auctionsResult),
        };
    }
    catch(error) {
        console.error(error);
        throw new createError.InternalServerError(error);
    }
}

module.exports.handler = commonMiddleware(getAuctions)
    .use(validator({ inputSchema: getAuctionsSchema, useDefaults: true }));
