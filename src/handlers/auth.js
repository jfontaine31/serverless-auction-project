const Jwt = require('../../lib/jwt');
const generatePolicy = require('../../lib/generate-policy');

module.exports.handler = async function handler(event, context) {
    if (!event.authorizationToken) {
        throw 'Unauthorized';
    }

    const token = event.authorizationToken.replace('Bearer ', '');

    try {
        const context = await Jwt.verify(token);
        const policy = generatePolicy(context.sub, event.methodArn);

        return { ...policy, context };
    } catch (error) {
        console.error(error);
        throw 'Unauthorized';
    }
};