const createError = require("http-errors");
const Auctions = require("../../controllers/auctions");
const NotifyService = require('../../lib/notify-service');


async function processAuctions(event, context) {
    try {
        const auctions = new Auctions();
        const auctionsToClose = await auctions.getEndedAuctions();

        const closedPromises = [];
        for(let i = 0; i < auctionsToClose.length;i++) {
            const auction = auctionsToClose[i];

            try {
                await auctions.closeAuction(auction);
                closedPromises.push(
                    NotifyService.notifySuccessfulAuction(auction)
                );
            } catch (error) {
                closedPromises.push(
                    NotifyService.notifyNoSeller(error.auction)
                )
            }
        }

        const closedAuctions = await Promise.all(closedPromises);
        const successfullyClosed = closedAuctions.filter(closedAuction => closedAuction);
        return { closed: successfullyClosed.length };
    }
    catch(error) {
        console.error(error)
        throw new createError.InternalServerError(error);
    }
}

module.exports.handler = processAuctions;