const Email = require('../../controllers/email');

async function sendMail(event, context) {
    const [record] = event.Records; // get the first item
    const emailBody = JSON.parse(record.body);
    const { recipient, subject, body } = emailBody;
    console.log(record);
    const email = new Email(recipient, subject, body);
    await email.sendMail();
    return event;
}

module.exports.handler = sendMail;