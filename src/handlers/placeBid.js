const statuses = require('statuses');
const createError = require('http-errors');
const validator = require("@middy/validator");
const Auctions = require("../../controllers/auctions");
const commonMiddleware = require("../../lib/common-middlewares");
const AUCTION_STATUSES = require('../../constants/auction-statuses');
const placeBidSchema = require('../../lib/schemas/placeBidSchema');

async function placeBid(event, context) {
    const { auctionId } = event.pathParameters;
    const { amount } = event.body;
    const { email } = event.requestContext.authorizer;

    const auctions = new Auctions();
    const auction = await auctions.getAuctionById(auctionId);
    if(!auction)
        throw new createError.NotFound(`AuctionID: ${auctionId} not found`);

    const {
        seller: auctionSeller,
        highestBid: {
            amount: auctionAmount,
            bidder: auctionBidder = ""
        },
        status: auctionStatus
    } = auction;

    if(auctionStatus === AUCTION_STATUSES.CLOSED) {
        throw new createError.Forbidden(`AuctionID: ${auctionId} is closed`);
    }

    if(auctionSeller === email) {
        throw new createError.Forbidden(`You cannot bid on your own auction`);
    }

    if(auctionBidder === email) {
        throw new createError.Forbidden(`You already have the highes bid`);
    }

    if(amount <= auctionAmount) {
        throw new createError.Forbidden(`You're bid must be higher than ${auctionAmount}!`);
    }

    try {
        const updatedAuction = await auctions.updateAuction(auctionId, amount, email);
        return {
            statusCode: statuses("OK"),
            body: JSON.stringify(updatedAuction),
        };
    }
    catch(error) {
        console.error(error);
        throw new createError.InternalServerError(error)
    }
}

module.exports.handler = commonMiddleware(placeBid)
    .use(validator({ inputSchema: placeBidSchema, useDefaults: true  }));
