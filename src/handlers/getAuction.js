const statuses = require('statuses');
const commonMiddleware = require("../../lib/common-middlewares");
const createError = require('http-errors');
const Auctions = require('../../controllers/auctions');

async function getAuction(event, context) {
    let auctionResult;
    const { auctionId } = event.pathParameters;
    try {
        const auctions = new Auctions();
        auctionResult = await auctions.getAuctionById(auctionId)
    }
    catch(error) {
        console.error(error);
        throw new createError.InternalServerError(error)
    }

    if(!auctionResult) {
        throw new createError.NotFound(`AuctionID: ${auctionId} not found`);
    }

    return {
        statusCode: statuses("OK"),
        body: JSON.stringify(auctionResult),
    };
}

module.exports.handler = commonMiddleware(getAuction);
