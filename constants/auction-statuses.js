
const AUCTION_STATUSES = {
    OPEN: "OPEN",
    CLOSED: "CLOSED"
}

module.exports = AUCTION_STATUSES;