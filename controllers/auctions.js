const { v4: uuid } = require('uuid');
const { AUCTIONS_TABLE_NAME } = require("../constants/dynamoDB");
const AUCTION_STATUSES = require("../constants/auction-statuses");
const dynamoDB = require("../lib/dynamoDB");

class Auctions {
    async createAuction(title, seller) {
        const now = new Date()
        const endDate = new Date();
        endDate.setHours(now.getHours() + 1)
        const auction = {
            id: uuid(),
            title,
            status: AUCTION_STATUSES.OPEN,
            createdAt: now.toISOString(),
            endingAt: endDate.toISOString(),
            highestBid: {
                amount: 0,
            },
            seller
        };

        await dynamoDB.put({
            TableName: AUCTIONS_TABLE_NAME,
            Item: auction
        }).promise();

        return auction;
    };

    async getAuctionById(auctionId) {
        const params = {
            TableName: AUCTIONS_TABLE_NAME,
            Key: { id: auctionId }
        };
        const { Item: auction }  = await dynamoDB.get(params).promise();
        return auction;
    }

    async updateAuction(auctionId, amount, bidder) {
        const params = {
            TableName: AUCTIONS_TABLE_NAME,
            Key: { id: auctionId },
            UpdateExpression: "set highestBid.amount = :amount, highestBid.bidder = :bidder",
            ExpressionAttributeValues: {
                ":amount": amount,
                ":bidder": bidder
            },
            ReturnValues: "ALL_NEW",
        };

        const result = await dynamoDB.update(params).promise()
        return result.Attributes
    }

    closeAuction(auction) {
        const { id } = auction;
        const params = {
            TableName: AUCTIONS_TABLE_NAME,
            Key: { id },
            UpdateExpression: "set #status = :status",
            ExpressionAttributeValues: {
                ":status": AUCTION_STATUSES.CLOSED
            },
            ExpressionAttributeNames: { "#status": "status" },
            ReturnValues: "ALL_NEW",
        }

        return dynamoDB.update(params).promise();
    }

    async getAuctionsByStatus(status) {
        const params = {
            TableName: AUCTIONS_TABLE_NAME,
            IndexName: "statusAndEndDate",
            KeyConditionExpression: "#status = :status",
            ExpressionAttributeValues: {
                ":status": status
            },
            ExpressionAttributeNames: {
                "#status": "status"
            }

        };
        const { Items } = await dynamoDB.query(params).promise();
        return Items;
    }

    async getEndedAuctions() {
        const now = new Date();
        const params = {
            TableName: AUCTIONS_TABLE_NAME,
            IndexName: "statusAndEndDate",
            KeyConditionExpression: '#status = :status AND endingAt <= :now', // #status is reserved word so need '#'
            ExpressionAttributeValues: {
                ":status": AUCTION_STATUSES.OPEN,
                ":now": now.toISOString()
            },
            ExpressionAttributeNames: {
                "#status": "status"
            }
        };

        const result = await dynamoDB.query(params).promise();
        return result.Items;
    }
}

module.exports = Auctions;
