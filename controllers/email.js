const AWS = require('aws-sdk');
const { AWS_REGION } = require('../constants/aws-variables');
const { SOURCE_EMAIL } = require('../constants/source-email');

const ses =  new AWS.SES({
    region: AWS_REGION
})

class Email {
    constructor(recipient, subject, body) {
        this.recipient = recipient;
        this.subject = subject;
        this.emailBody = body;
    }
    async sendMail() {
        const params = {
            Source: SOURCE_EMAIL,
            Destination: {
                ToAddresses: [
                    this.recipient
                ]
            },
            Message: {
                Body: {
                    Text: {
                        Data: this.emailBody
                    }
                },
                Subject: {
                    Data: this.subject
                }
            }
        }

        try {
            const result = await ses.sendEmail(params).promise();
        }
        catch(error) {
            console.error(error)
            throw error;
        }
    }
}

module.exports = Email;